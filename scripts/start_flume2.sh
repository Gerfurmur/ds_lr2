#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR
DATA_NAME=$1


export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JAVA_OPTS="-Xms100m -Xmx2000m -Dcom.sun.management.jmxremote"
export JAVA_OPTS="$JAVA_OPTS -Dorg.apache.flume.log.rawdata=true -Dorg.apache.flume.log.printconfig=true "
export FLUME_HOME=$SCRIPT_DIR/apache-flume-1.9.0-bin
export PATH=$PATH:$FLUME_HOME/bin
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop


flume-ng agent --conf $FLUME_HOME/conf --conf-file $SCRIPT_DIR/../flume_data.conf --name a1 -Dflume.root.logger=INFO,console &

sleep 5s

nc 127.0.0.1 11111 < $SCRIPT_DIR/../tmp/$DATA_NAME.in & 
