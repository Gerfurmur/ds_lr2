#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR

export SPARK_HOME=$SCRIPT_DIR/spark-3.1.3-bin-hadoop3.2
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export PATH=$PATH:$SPARK_HOME/bin

spark-submit --class bdtc.lab2.SparkRDD_App --master local --deploy-mode client --executor-memory 1g --name flightcount --conf "spark.app.id=SparkRDD_App" $SCRIPT_DIR/../target/lab2-1.0-SNAPSHOT-jar-with-dependencies.jar /data/ /out /catalog/ 1
 
