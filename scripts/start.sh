#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR

if [[ $# -lt 4 ]] ; then
  DATA_NAME="data"
  SCALE=1
  AMOUNT=50
  TIMERANGE=2
else
  DATA_NAME=$1
  SCALE=$2
  AMOUNT=$3
  TIMERANGE=$4
fi

export PATH=$PATH:/home/hdoop/hadoop-3.2.2/bin/
hdfs dfs -rm -r /catalog
hdfs dfs -rm -r /data
hdfs dfs -mkdir /catalog
hdfs dfs -mkdir /data
hdfs dfs -rm -r /out
# Устанавливаем mimesis
pip3 install mimesis

# Генерируем входные данные и добавляем их в таблицу
sudo python3 $SCRIPT_DIR/../input_data_generator.py $DATA_NAME $AMOUNT $TIMERANGE

export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JAVA_OPTS="-Xms100m -Xmx2000m -Dcom.sun.management.jmxremote"
export JAVA_OPTS="$JAVA_OPTS -Dorg.apache.flume.log.rawdata=true -Dorg.apache.flume.log.printconfig=true "

# install flume
if [ ! -f apache-flume-1.9.0-bin.tar.gz ]; then
    wget https://dlcdn.apache.org/flume/1.9.0/apache-flume-1.9.0-bin.tar.gz
    tar xaf apache-flume-1.9.0-bin.tar.gz
else
    echo "Flume already exists, skipping..."
fi

#export FLUME_HOME=$SCRIPT_DIR/apache-flume-1.9.0-bin
#export PATH=$PATH:$FLUME_HOME/bin
rm -f $FLUME_HOME/lib/guava-11.0.2.jar

# download Spark
if [ ! -f spark-3.1.3-bin-hadoop3.2.tgz ]; then
    wget https://dlcdn.apache.org/spark/spark-3.1.3/spark-3.1.3-bin-hadoop3.2.tgz
    tar xaf spark-3.1.3-bin-hadoop3.2.tgz
else
    echo "Spark already exists, skipping..."
fi

#export SPARK_HOME=$SCRIPT_DIR/spark-3.1.3-bin-hadoop3.2
#export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop

# flume get data and save in hdfs
#flume-ng agent --conf $FLUME_HOME/conf --conf-file $SCRIPT_DIR/../flume_catalog.conf --name a1 -Dflume.root.logger=INFO,console &
#sleep 5s
#nc 127.0.0.1 11112 < $SCRIPT_DIR/../tmp/$DATA_NAME.catalog
#sleep 1m
#flume-ng agent --conf $FLUME_HOME/conf --conf-file $SCRIPT_DIR/../flume_data.conf --name a1 -Dflume.root.logger=INFO,console &

#sleep 5s

#nc 127.0.0.1 11111 < $SCRIPT_DIR/../tmp/$DATA_NAME.in &

#sleep 3m

#export PATH=$PATH:$SPARK_HOME/bin

#spark-submit --class bdtc.lab2.SparkRDD_App --master local --deploy-mode client --executor-memory 1g --name flightcount --conf "spark.app.id=SparkRDD_App" $SCRIPT_DIR/../target/lab2-1.0-SNAPSHOT-jar-with-dependencies.jar /data/ /out /catalog/ $SCALE

cd -

