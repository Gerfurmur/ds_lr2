import subprocess
import os
import sys
from time import sleep
import multiprocessing


THIS_PATH = os.path.dirname(os.path.abspath(__file__))


def run_cmd(cmd):
    
    try:
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
        out, err = proc.communicate()
        ret = proc.returncode
    except Exception:
        out, err, ret = b'', b'1', 0
    
    return out.decode(), err.decode(), ret


def install(_fname_pat, _scale, _amount, _timerange):
    cmd = "%s/scripts/start.sh %s %s %s %s" % (THIS_PATH, _fname_pat, _scale, _amount, _timerange)
    print(run_cmd(cmd))


def start_flume1(_fname_pat):
    cmd = "%s/scripts/start_flume1.sh %s" % (THIS_PATH, _fname_pat)
    print(run_cmd(cmd))


def start_flume2(_fname_pat):
    cmd = "%s/scripts/start_flume2.sh %s" % (THIS_PATH, _fname_pat)
    print(run_cmd(cmd))


def start_spark():
    cmd = "%s/scripts/start_spark.sh" % THIS_PATH
    print(run_cmd(cmd))


def arg_parse():
    print("USAGE: python3 start.py <input data filename> <scale> <amount of generating data outer loop> <timerange>")
    # FIXME
    try:
        _fname_pat = sys.argv[1]
    except IndexError:
        _fname_pat = "input_data"

    try:
        _scale = sys.argv[2]
    except IndexError:
        _scale = "1"

    try:
        _amount = sys.argv[3]
    except IndexError:
        _amount = "100"

    try:
        _timerange = sys.argv[4]
    except IndexError:
        _timerange = "10"
    return _fname_pat, _scale, _amount, _timerange


def main():
    _fname_pat, _scale, _amount, _timerange = arg_parse()
    i = multiprocessing.Process(target=install, args=[_fname_pat, _scale, _amount, _timerange])
    f1 = multiprocessing.Process(target=start_flume1, args=[_fname_pat])
    f2 = multiprocessing.Process(target=start_flume2, args=[_fname_pat])
    s = multiprocessing.Process(target=start_spark)
    
    i.start()
    i.join()
    
    f1.start()
    sleep(60)
    f2.start()
    sleep(180)
    s.start()
    s.join()
    f1.join()
    f2.join()
    

if __name__ == "__main__":
    main()
    
    
    
    
