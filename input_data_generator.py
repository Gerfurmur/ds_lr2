import mimesis
import datetime
import random
import os
import subprocess
#import argsparser
import sys


THIS_PATH = os.path.abspath(os.path.dirname(__file__))


def generate(output_file, catalog, amount=1000, timerange=10):
    gen2 = mimesis.Address()
    
    now = int(datetime.datetime.now().timestamp() * 1000)
    
    catalog_d = {}
    for i in range(amount * 10):
        catalog_d[gen2.address()] = gen2.country()
    
    tmp_keys = [k.replace(',', '') for k in catalog_d.keys()]
    
    with open(output_file, 'w') as wf:
        for i in range(amount):
            
            for j in range(1000):
                data = [str(1000 * i + j), str(now + random.randint(0, 60 * 60 * 1000 * timerange)), 
                        random.choice(tmp_keys), random.choice(tmp_keys)]
                
                wf.write(','.join(data) + '\n')
                
    
    with open(catalog, 'w') as wf:
        wf.write('\n'.join("%s,%s" % (key, catalog_d[key]) for key in catalog_d) + '\n')


def arg_parse():
    print("USAGE: python3 input_data_generator.py <input data filename> <amount of generating data outer loop> <timerange>")
    # FIXME
    try:
        _fname_pat = sys.argv[1]
    except IndexError:
        _fname_pat = "input_data"

    try:
        _amount = sys.argv[2]
    except IndexError:
        _amount = "100"

    try:
        _timerange = sys.argv[3]
    except IndexError:
        _timerange = "10"
    return _fname_pat, _amount, _timerange



if __name__ == "__main__":
    _fname_pat, _amount, _timerange = arg_parse()
    
    os.makedirs(os.path.join(THIS_PATH, "tmp"), exist_ok=True)
    
    output_file = os.path.abspath(os.path.join(THIS_PATH, "tmp", _fname_pat + ".in"))
    catalog = os.path.abspath(os.path.join(THIS_PATH, "tmp", _fname_pat + ".catalog"))
    
    generate(output_file, catalog, int(_amount), int(_timerange) + 1)
    


