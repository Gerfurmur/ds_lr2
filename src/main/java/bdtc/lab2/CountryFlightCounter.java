package bdtc.lab2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import scala.Tuple2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.Map;

import static java.time.temporal.ChronoField.YEAR;


@Slf4j
public class CountryFlightCounter {

    public static JavaPairRDD<String, Integer> countFlightsPerScale(JavaRDD<String> inputRDD, Map<String,String> catalog, long scale) {
        log.info("SCALE----------------------" + scale);


        // split strings -> 1: "flight_number,time,destination_airport,departure_airport", 2:...
        JavaRDD<String> lines = inputRDD.map(s -> Arrays.toString(s.split("\n")));

        return lines
                .map(s -> {
                    String[] words = s.split(",");
                    log.info("-----------------------\n" + s);
                    long dep_time = Long.parseLong(words[1]);
                    dep_time = dep_time - dep_time % (scale * 60L * 60L * 1000L);
                    log.info("get time " + dep_time);
                    log.info("-----------------------\n" + dep_time + "," + catalog.get(words[3].replace("]", "")) + "," + catalog.get(words[2]));
                    return Long.toString(dep_time) + "," + catalog.get(words[3].replace("]", "")) + "," + catalog.get(words[2]);
                })
                .mapToPair(x -> new Tuple2<String, Integer>(x, 1))
                .reduceByKey(Integer::sum);
    }

}
