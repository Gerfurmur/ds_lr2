package bdtc.lab2;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.SparkSession;
import org.apache.hadoop.conf.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Считает количество событий syslog разного уровная log level по часам.
 */
@Slf4j
public class SparkRDD_App {
    /*public static void parseCatalog(String catalog, Map<String,String> conf) {
        // catalog contain strings : "airport,country"
        try {
            FileSystem fileSystem = FileSystem.get(new Configuration());
            Path path = new Path(catalog);
            FSDataInputStream in = fileSystem.open(path);

            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line = null;

            while((line = br.readLine())!= null) {
                // parse data in catalog, where each line - id,name
                try {
                    conf.put(line.split(",")[0], line.split(",")[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            conf.put("Main airport", "UFA"); // default value for tests
        }
    }*/
    public static void parseCatalog(JavaRDD<String> catalog, Map<String,String> conf) {
        // catalog contain strings : "airport,country"
        try {
            for (String line : catalog.collect()) {
                try {
                    conf.put(line.split(",")[0], line.split(",")[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            conf.put("Main airport", "UFA"); // default value for tests
        }
    }


    /**
     * @param args - args[0]: входной файл, args[1] - выходная папка
     */
    public static void main(String[] args) {
        if (args.length < 3) {
            throw new RuntimeException("Usage: java -jar SparkRDD_App.jar <input.file> <outputDirectory> <catalog> [<hour scale>]");
        }

        log.info("Appliction started!");
        //log.debug("Application started");
        SparkSession spark = SparkSession
                .builder()
                .master("local")
                .appName("SparkRDD_App")
                .getOrCreate();

        // parse catalog
        Map<String,String> dictionary = new HashMap<String,String>();
        JavaRDD<String> catalog = spark.sparkContext().textFile(args[2], 1).toJavaRDD();

        long scale = 1L;

        if (args.length == 4) {
            scale = Long.parseLong(args[3]);
        }

        //log.info("-------------------\n" + spark.sparkContext().textFile(args[2], 1).isEmpty() + "---------------------\n");
        //parseCatalog(args[2], dictionary);
        parseCatalog(catalog, dictionary);

        String data = dictionary.toString();
        log.info("CONF DATA \n" + data + "\n");

        // create rdd with min 3 partitions
        JavaRDD<String> file = spark.sparkContext().textFile(args[0], 3).toJavaRDD();

        // analyse data
        log.info("===============COUNTING...================");
        JavaPairRDD<String, Integer> results = CountryFlightCounter.countFlightsPerScale(file, dictionary, scale);

        // save results
        log.info("============SAVING FILE TO " + args[1] + " directory============");
        results.saveAsTextFile(args[1]);
    }
}
