package bdtc.lab2;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static bdtc.lab2.CountryFlightCounter.countFlightsPerScale;

public class SparkTest {

    final String testString1 = "12,1547419423601,Moscow,LA\n";
    final String testString2 = "13,1647419423601,LA,Moscow\n";
    final String testString3 = "14,1647419423601,LA,Moscow\n";
    final String resStringOne = "1547416800000,USA,Russia";
    final String resStringTwoSame = "1647417600000,Russia,USA";

    SparkSession ss = SparkSession
            .builder()
            .master("local")
            .appName("SparkSQLApplication")
            .getOrCreate();
    Map<String,String> dict= new HashMap<String,String>();



    @Before
    public void setUp() {
        dict.put("Moscow", "Russia");
        dict.put("LA", "USA");
    }

    @Test
    public void testOneStr() {

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> dudu = sc.parallelize(Arrays.asList(testString1));
        JavaPairRDD<String, Integer> result = countFlightsPerScale(dudu, dict, 1L);

        assert result.collect().iterator().next()._1.equals(resStringOne);
        assert result.collect().iterator().next()._2.equals(1);
    }

    @Test
    public void testTwoStrsSameTime(){

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> dudu = sc.parallelize(Arrays.asList(testString3, testString2));
        JavaPairRDD<String, Integer> result = countFlightsPerScale(dudu, dict, 1L);

        assert result.collect().iterator().next()._1.equals(resStringTwoSame);
        assert result.collect().iterator().next()._2.equals(2);
    }

    @Test
    public void testTwoStrsDifferentTime(){

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> dudu = sc.parallelize(Arrays.asList(testString1, testString2));
        JavaPairRDD<String, Integer> result = countFlightsPerScale(dudu, dict, 1L);

        assert result.collect().get(1)._1.equals(resStringOne);
        assert result.collect().get(1)._2.equals(1);

        assert result.collect().get(0)._1.equals(resStringTwoSame);
        assert result.collect().get(0)._2.equals(1);
    }

    @Test
    public void testThreeStrs() {

        JavaSparkContext sc = new JavaSparkContext(ss.sparkContext());
        JavaRDD<String> dudu = sc.parallelize(Arrays.asList(testString1, testString2, testString3));
        JavaPairRDD<String, Integer> result = countFlightsPerScale(dudu, dict, 1L);

        assert result.collect().get(1)._1.equals(resStringOne);
        assert result.collect().get(1)._2.equals(1);

        assert result.collect().get(0)._1.equals(resStringTwoSame);
        assert result.collect().get(0)._2.equals(2);
    }

}
